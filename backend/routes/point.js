const express = require('express');
const router = express.Router();
//
// Load controller
const point_controller = require('../controllers/point');
//
//Get all
router.get('', point_controller.all);
//Get all of type
router.get('/type/:type', point_controller.alloftype);
//Get single
router.get('/:id', point_controller.details);
//Create
router.post('', point_controller.create);
//Update
router.put('/:id', point_controller.update);
//Delete
router.delete('/:id', point_controller.delete);


module.exports = router;