const express = require('express');
const router = express.Router();
//
// Load controller
const freight_controller = require('../controllers/freight');
//
//Get all
router.get('', freight_controller.all);
//Get single
router.get('/:id', freight_controller.details);
//Create
router.post('', freight_controller.create);
//Update
router.put('/:id', freight_controller.update);
//Delete
router.delete('/:id', freight_controller.delete);


module.exports = router;