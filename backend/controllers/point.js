const Point = require('../models/point');
//
exports.all = function (req, res) {
	Point.find({}, function (err, result) {
        if (err){
            let response = {
				"message" : "Points fetch failed. "+err.errmsg,
				"data" : [{}]
			}
			res.set('Content-Type', 'application/json; charset=utf-8');
			//Set response status code
			res.status(404);
			//Respond
			res.send(response);
        }
        else{
        	let response = {
				"message" : "Points fetched successfully. ",
				"data" : JSON.stringify(result)
			}
			res.set('Content-Type', 'application/json; charset=utf-8');
			//Set OK status code
			res.status(200);
			//Respond
			res.send(response);
        }
    });
};

exports.alloftype = function (req, res) {
	Point.find({type: req.params.type}, function (err, result) {
        if (err){
            let response = {
				"message" : "Points fetch failed. "+err.errmsg,
				"data" : [{}]
			}
			res.set('Content-Type', 'application/json; charset=utf-8');
			//Set response status code
			res.status(404);
			//Respond
			res.send(response);
        }
        else{
        	let response = {
				"message" : "Points fetched successfully. ",
				"data" : JSON.stringify(result)
			}
			res.set('Content-Type', 'application/json; charset=utf-8');
			//Set OK status code
			res.status(200);
			//Respond
			res.send(response);
        }
    });
};

exports.create = function (req, res) {
	let point = new Point({
			name: req.body.name,
			type: req.body.type,
			latitude: req.body.latitude,
			longitude: req.body.longitude
		});
	point.save().then(doc => {
		//console.log(doc);
		let response = {
			"message" : "Point Created successfully",
			"data" : doc
		}
		//Response status 201 for OK and something was created
		res.status(201);
		res.set('Content-Type', 'application/json; charset=utf-8');
		//or res.json(response). Not always dependable to set response content-type and character encoding
		res.send(response);
	}).catch(err => {
		console.error(err);
		//Respond with error 400 for unprocessable or suppositional data
		let response = {
			"message" : "Point Creation failed. "+err.errmsg,
			"data" : {}
		}
		res.set('Content-Type', 'application/json; charset=utf-8');
		//Set response status code for bad request
		res.status(400);
		//Respond
		res.send(response);
	});
};

exports.details = function (req, res) {
	Point.findById(req.params.id, function (err, point) {
		//set respond content type
		res.set('Content-Type', 'application/json; charset=utf-8');
		//Check for error and respond appropriately
		if (err){
			console.error(err);
			//Respond with error
			let response = {
				"message" : "Point details error. "+err.errmsg,
				"data" : point
			}
			//Assume not found until better error handling logic comes to mind
			res.status(404);
			//Respond
			res.send(response);
		}
		else{
			let response = {
				"message" : "Point found.",
				"data" : point
			}
			//Dont leave status to chance
			res.status(200);
			//Respond
			res.send(response);
		}
	});
};


exports.update = function (req, res) {
	//Udate or insert if not exists
	Point.updateOne({_id:req.params.id}, {$set: req.body}, {upsert: true}).then((obj) => {
		//set response content type and encoding
		res.set('Content-Type', 'application/json; charset=utf-8');
		//Get and return updated record
			Point.findById(req.params.id, function (err, point) {
				if (err) {
					//Respond with success any way
					let response = {
						"message" : "Point \""+req.params.id+"\" updated.",
						"data" : result
					}
					//set OK status
					res.status(200);
					//Respond
					res.send(response);
				}
				else{
					//Respond with success response
					let response = {
						"message" : "Point \""+req.params.id+"\" updated.",
						"data" : point
					}
					//set OK status
					res.status(200);
					//Respond
					res.send(response);
				}
			});			

	}).catch(err => {
		//set response content type and encoding
		res.set('Content-Type', 'application/json; charset=utf-8');
		let response = {
				"message" : "Point \""+req.params.id+"\" update failed. "+err.errmsg,
				"data" : {}
			}
		//Assume unprocessable request body
		res.status(422);
		//Respond
		res.send(response);
	});
};

exports.delete = function (req, res) {
	Point.findByIdAndRemove(req.params.id, function (err) {
		//set response content type and encoding
		res.set('Content-Type', 'application/json; charset=utf-8');
		if (err) {
			console.error(err);
			//Respond with error response
			let response = {
				"message" : "Point \""+req.params.id+"\" deletion failed. "+err.errmsg,
				"data" : {}
			}
			//Assume not found
			res.status(404);
			//Respond
			res.send(response);
		}
		else{
			//Respond with error response
			let response = {
				"message" : "Point \""+req.params.id+"\" deleted.",
				"data" : {}
			}
			//set OK status for no content
			res.status(204);
			//Respond
			res.send(response);
		}
	});
};