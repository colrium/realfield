const mongoose = require('mongoose');
const Schema = mongoose.Schema;
//
//Freight points schema
let FreightSchema = new Schema({
	pickup_point: {
		type: Schema.ObjectId, 
		ref: 'Point', 
		required: true
	},
	dropoff_point: {
		type: Schema.ObjectId, 
		ref: 'Point', 
		required: true
	},
	vehicle_plate: {
		type: String,
		required: true		
	},
	driver: {
		type: String
	},
	description: {
		type: String
	}
});
//
// Export model
module.exports = mongoose.model('Freight', FreightSchema);