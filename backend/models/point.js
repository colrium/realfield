const mongoose = require('mongoose');
const Schema = mongoose.Schema;
//
//Freight points schema
let PointSchema = new Schema({
	name: {
		type: String, 
		required: true,
		max: 255
	},
	type: {
		type: String,
		enum: ['pickup', 'dropoff'],
		required: true, 
		defult: 'pickup'
	},
	latitude: {
		type: Number,
		required: true
	},
	longitude: {
		type: Number,
		required: true
	},
});
//
// Export model
module.exports = mongoose.model('Point', PointSchema);
