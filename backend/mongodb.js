const mongoose = require('mongoose');
const config =  require('./config');
// Set up mongoose connection
const mongoDB = process.env.MONGODB_URI || config.mongo_db_url;
//Set the following properties to avoid deprecation warnings
mongoose.set('useNewUrlParser', true);
mongoose.set('useFindAndModify', false);
mongoose.set('useCreateIndex', true);
//
mongoose.connect(mongoDB);
mongoose.Promise = global.Promise;

//
exports.connection = mongoose.connection;