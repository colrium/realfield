//Requirements
const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const cors = require('cors');
const config =  require('./config');
const db =  require('./mongodb').connection;
db.on('error', console.error.bind(console, 'MongoDB connection error:'));
//
//ToDo:- jQuery initialization
//

// init express app
const app = express();
//Middleware
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));
//Cors for cross origin errors
app.use(cors());
//
app.use(express.static('public'));
//
//Routes
const point = require('./routes/point');
const freight = require('./routes/freight');
app.use('/points', point);
app.use('/freights', freight);
//
//Define Listening port
let port = process.env.PORT || config.port;
//Listen on port
app.listen(port, () => {
	console.log('Server is up and listening on port : ' + port);
});

