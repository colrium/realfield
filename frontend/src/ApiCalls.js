import axios from 'axios';
const API_URL = 'http://localhost:3000';

export class ApiCalls{

	static getPoints() {
		return new Promise(async (resolve, reject) => {
			const url = API_URL+'/points';
			try {
				const res = await axios.get(url);
				const data = JSON.parse(res.data.data);
				resolve(data);
			} catch(err){
				reject(err);
			}
		})
	}
	static getPointsOfType(type) {
		return new Promise(async (resolve, reject) => {
			const url = API_URL+'/points/type/'+type;
			try {
				const res = await axios.get(url);
				const data = JSON.parse(res.data.data);
				resolve(data);
			} catch(err){
				reject(err);
			}
		})
	}
	static getPoint(pk) {
		return new Promise(async (resolve, reject) => {
			const url = API_URL+'/points/'+pk;

			try {
				const res = await axios.get(url);
				const data = res.data.data;

				resolve(data);
			} catch(err){
				reject(err);
			}
		});
	}
	static deletePoint(point){
		const url = API_URL+'/points/${point.pk}';
		return axios.delete(url);
	}

	static createPoint(point){
		return new Promise(async (resolve, reject) => {
			const url = API_URL+'/points';
			try {
				const res = await axios.post(url,point);
				const data = JSON.parse(res.data.data);
				resolve(data);
			} catch(err){
				reject(err);
			}
		})
		
		
	}

	static updatePoint(point){
		const url = API_URL+'/points/${point.pk}';
		return axios.put(url,point);
	}
	/*
	* 
	* Freight calls
	* 
	*/
	static getFreights() {
		return new Promise(async (resolve, reject) => {
			const url = API_URL+'/freights';
			try {
				const res = await axios.get(url);
				const data = JSON.parse(res.data.data);
				console.log("data", data)
				resolve(data);
			} catch(err){
				reject(err);
			}
		})
	}
	static getFreight(id) {
		const url = API_URL+'/freights/${id}';
		return axios.get(url).then(response => response.data);
	}
	static deleteFreight(freight){
		const url = API_URL+'/freights/${freight._id}';
		return axios.delete(url);
	}

	static createFreight(freight){
		return new Promise(async (resolve, reject) => {
			const url = API_URL+'/freights/';
			try {
				const res = await axios.post(url,freight);
				const data = JSON.parse(res.data.data);
				resolve(data);
			} catch(err){
				reject(err);
			}
		});
	}

	static updateFreight(freight){
		const url = API_URL+'/freights/${freight._id}';
		return axios.put(url,freight);
	}
}