# Realfield.io

Realfield senior software developer: Programming challenge.
	Backend:
		MongoDb
		Express js
		Mongoose

	Frontend:
		Vue js
		Here Maps

	Some excemptions i made due to limitation in time
		Update
		Delete 
		Validation
		Maps
		DataTables

1. Installation
	In a terminal cd to both this directory and ./frontend directory and execute
	``` 
	npm i
	```
2. Backend Configuration
   To change backend port and mongodb url edit ./backend/config.js appropriately
   currenctly backend is configured to run on port 3000

3. Frontend configuration
	Incase you change the backend listening port please change const line 2 : "API_URL = 'http://localhost:3000';" of  ./frontend/src/ApiCalls.js to match your changes

4. Run 
	from the directory path of this file execute
	```
	npm run dev
	```
	This will spin up a backend server at http://localhost:3000 frontend server that can be accessed at http://localhost:8080